#!/usr/bin/env Rscript
rmarkdown::render("source/presentation.Rmd", output_dir = "build", output_file = "index.html")
rmarkdown::render("source/handout.Rmd")
system("mv source/handout.pdf build/")
