# HeFDI Forschungsdatentag 2021 Versionskontrolle

Vortragsfolien für den Paralleltalk zum HeFDI Forschungsdatentag 2021 über Versionskontrolle im FDM

## Download

- [Präsentation anschauen][presentation]
- [Handout lesen][handout]

[presentation]: https://cookt.gitlab.io/hefdi-forschungsdatentag-2021-versionskontrolle
[handout]: https://cookt.gitlab.io/hefdi-forschungsdatentag-2021-versionskontrolle/handout.pdf
